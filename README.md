J'ai décidé:
	- d'avoir un retour d'erreur plus détaillé que ce que demande le sujet (en tant que bonus)
	- d'ecrire la carte au fur et a mesure que je la lis afin de voir quelle est la ligne fausse / a quelle ligne on arrete la lecture
	- d'accepter les rooms aux memes coordonnees
	- d'accepter plusieurs rooms end ou start (la derniere ecrasant la premiere)
	- de ne pas accepter les commentaires suivant ##start et ##end
