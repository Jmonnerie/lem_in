/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_width.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/16 01:35:13 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:34 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_width(t_printf *lst, char **str, va_list ap)
{
	if (**str != '*')
	{
		lst->width = ft_atoi(*str);
		while (**str <= '9' && **str >= '0')
			*str += 1;
		return ;
	}
	lst->width = va_arg(ap, long long int);
	if (lst->width < 0)
	{
		lst->width *= -1;
		lst->flag[1] = 1;
	}
	*str += 1;
}
