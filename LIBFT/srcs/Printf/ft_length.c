/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_length.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/16 01:51:04 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:33 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_length(t_printf *lst, char **str)
{
	if (**str == 'h' && *(*str + 1) == 'h' && lst->length == 0)
		lst->length = 'H';
	if (**str == 'h' && *(*str + 1) != 'h' && (lst->length == 'H' ||
			lst->length == 0))
		lst->length = 'h';
	if (**str == 'j' && lst->length != 'z')
		lst->length = 'j';
	if (**str == 'z')
		lst->length = 'z';
	if (**str == 'l' && *(*str + 1) != 'l' && lst->length != 'L' &&
			lst->length != 'j' && lst->length != 'z')
		lst->length = 'l';
	if (**str == 'l' && *(*str + 1) == 'l' && lst->length != 'z' &&
			lst->length != 'j')
		lst->length = 'L';
	if (lst->length == 'H' || lst->length == 'L')
		*str += 2;
	else
		*str += 1;
}
