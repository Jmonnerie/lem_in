/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_colors.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/26 07:59:14 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:32 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_getcolor(char *str)
{
	if (!ft_strncmp(str, "{eoc}", 5))
		return (NORMAL);
	if (!ft_strncmp(str, "{bold}", 6))
		return (BOLD);
	if (!ft_strncmp(str, "{unbold}", 8))
		return (UNBOLD);
	if (!ft_strncmp(str, "{underline}", 11))
		return (UNDERLINE);
	if (!ft_strncmp(str, "{black}", 7))
		return (TBLACK);
	if (!ft_strncmp(str, "{red}", 5))
		return (TRED);
	if (!ft_strncmp(str, "{green}", 7))
		return (TGREEN);
	if (!ft_strncmp(str, "{yellow}", 8))
		return (TYELLOW);
	if (!ft_strncmp(str, "{blue}", 6))
		return (TBLUE);
	if (!ft_strncmp(str, "{purple}", 8))
		return (TMAGENTA);
	if (!ft_strncmp(str, "{cyan}", 6))
		return (TCYAN);
	if (!ft_strncmp(str, "{white}", 7))
		return (TWHITE);
	return (NULL);
}

int			ft_colors(char **str)
{
	char	*color;
	short	ret;

	if ((color = ft_getcolor(*str)))
	{
		ret = write(1, color, ft_strlen(color));
		while (**str != '}')
			*str += 1;
		return (ret);
	}
	else
	{
		ft_putchar(**str);
		return (1);
	}
}
