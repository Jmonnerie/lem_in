/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmpercent.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/02/28 18:01:57 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/16 17:07:31 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void		ft_cmpercent(t_printf *lst)
{
	int		i;

	i = -1;
	lst->plen = (lst->width ? lst->width : 1);
	if (!(lst->print = ft_strcnew(lst->plen, ' ')))
		return ;
	if (lst->flag[4] && !lst->flag[1])
		while (lst->print[++i])
			lst->print[i] = '0';
	lst->print[(lst->flag[1] ? 0 : lst->plen - 1)] = '%';
}
