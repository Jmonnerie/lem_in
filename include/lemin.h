/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lemin.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/05/02 17:33:28 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/07/25 19:57:20 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H
# include "../LIBFT/include/libft.h"

typedef struct			s_room
{
	char				*name;
	int					x;
	int					y;
	long				ant;
	uint8_t				visited:1;
	int					npipes;
	int					floor;
	struct s_room		*next;
	struct s_room		**pipes;
}						t_room;

typedef struct			s_solve
{
	int					nbways;
	int					nbturn;
	uint8_t				copied;
	int					*len;
	t_room				***tab;
	struct s_solve		*next;
}						t_solve;

typedef struct			s_lemin
{
	t_room				*rooms;
	t_room				*start;
	t_room				*end;
	t_solve				*solutions;
	uint8_t				direct:1;
	int					bottleneck;
	int					max;
	int					nbroom;
	int					ant;
}						t_lemin;

/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                BIONIC_ANT                                  ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int						lin_free(t_lemin *lin, int ret);
int						lin_free_solutions(t_solve *sol);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                                LIN_ENGINE                                  ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int						lin_engine(t_lemin *lin);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ SOLVE                          LIN_SOLVE                                   ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int						lin_launcher_recurse(t_lemin *lin);
int						lin_dijkstra(t_lemin *lin, t_room *current,
						t_room **tab, int *len);
int						lin_recurse(t_lemin *lin, int i);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ READ                           LIN_READ                                    ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int						lin_read(t_lemin *lin);
int						lin_read_ants(t_lemin *lin);
int						lin_comment(t_lemin *lin, char **line);
int						lin_read_rooms(t_lemin *lin, char *line);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ READ                           LIN_TESTS                                   ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int						lin_is_a_room(char *line);
int						lin_is_a_pipe(char *line);
int						lin_enought_infos(t_lemin *lin);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ READ                           LIN_GET                                     ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
t_room					*lin_get_room(t_lemin *lin, char **line);
int						lin_get_pipe(t_lemin *lin, char **line);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ OUTPUT                        LIN_OUTPUT                                   ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
int						lin_output(t_lemin *lin, t_room ***sol);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ UTILS                         LIN_UTILS                                    ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
t_room					*lin_get_room_in_lst(t_room *lst, char *name);
int						lin_calc_turns(int ants, t_solve *sol, int nbways);
t_solve					*lin_copy_solution(t_solve *sol);
int						lin_resize_pipes(t_room *rooma, t_room *roomb);
/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃ UTILS                         LIN_INIT                                     ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/
t_solve					*lin_init_solve(int bottleneck);
int						lin_init_pipes(t_lemin *lin);
t_room					*lin_init_room(char *name, int x, int y);
int						lin_init(t_lemin **lin);

#endif
