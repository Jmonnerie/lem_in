# **************************************************************************** #
#                                                            LE - /            #
#                                                                /             #
#     Makefile                                         .::    .:/ .      .::   #
#                                                   +:+:+   +:    +:  +:+:+    #
#     By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     #
#                                                   #+#   #+    #+    #+#      #
#     Created: 2018/05/02 17:36:05 by jmonneri     #+#   ##    ##    #+#       #
#     Updated: 2018/07/24 19:32:56 by fablin      ###    #+. /#+    ###.fr     #
#                                                           /                  #
#                                                          /                   #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = lem-in
MAP = test/valid_maps_part_1/400_map

CC = gcc
CC_FLAGS = -Wall -Wextra -Werror

PATH_SRC = ./srcs/
PATH_OBJ = ./objs/
PATH_INC = ./include/
INCS = $(addprefix $(PATH_INC), lemin.h)

#******************************************************************************#
#                                    LEM-IN                                    #
#******************************************************************************#

PATH_SRC_LEM-IN = $(PATH_SRC)lem-in/
PATH_OBJ_LEM-IN = $(PATH_OBJ)lem-in/
FILES_LEM-IN = $(shell ls $(PATH_SRC_LEM-IN) | grep '\.c' | cut -d "." -f 1)
OBJ_LEM-IN = $(addprefix $(PATH_OBJ_LEM-IN), $(addsuffix .o, $(FILES_LEM-IN)))
SRC_LEM-IN = $(addprefix $(PATH_SRC_LEM-IN), $(addsuffix .c, $(FILES_LEM-IN)))

#******************************************************************************#
#                                    READ                                      #
#******************************************************************************#

PATH_SRC_READ = $(PATH_SRC)read/
PATH_OBJ_READ = $(PATH_OBJ)read/
FILES_READ = lin_get lin_read lin_tests
OBJ_READ = $(addprefix $(PATH_OBJ_READ), $(addsuffix .o, $(FILES_READ)))
SRC_READ = $(addprefix $(PATH_SRC_READ), $(addsuffix .c, $(FILES_READ)))

#******************************************************************************#
#                                    SOLVE                                     #
#******************************************************************************#

PATH_SRC_SOLVE = $(PATH_SRC)solve/
PATH_OBJ_SOLVE = $(PATH_OBJ)solve/
FILES_SOLVE = $(shell ls $(PATH_SRC_SOLVE) | grep '\.c' | cut -d "." -f 1)
OBJ_SOLVE = $(addprefix $(PATH_OBJ_SOLVE), $(addsuffix .o, $(FILES_SOLVE)))
SRC_SOLVE = $(addprefix $(PATH_SRC_SOLVE), $(addsuffix .c, $(FILES_SOLVE)))

#******************************************************************************#
#                                    OUTPUT                                    #
#******************************************************************************#

PATH_SRC_OUTPUT = $(PATH_SRC)output/
PATH_OBJ_OUTPUT = $(PATH_OBJ)output/
FILES_OUTPUT = $(shell ls $(PATH_SRC_OUTPUT) | grep '\.c' | cut -d "." -f 1)
OBJ_OUTPUT = $(addprefix $(PATH_OBJ_OUTPUT), $(addsuffix .o, $(FILES_OUTPUT)))
SRC_OUTPUT = $(addprefix $(PATH_SRC_OUTPUT), $(addsuffix .c, $(FILES_OUTPUT)))

#******************************************************************************#
#                                    UTILS                                     #
#******************************************************************************#

PATH_SRC_UTILS = $(PATH_SRC)utils/
PATH_OBJ_UTILS = $(PATH_OBJ)utils/
FILES_UTILS = lin_init lin_utils
OBJ_UTILS = $(addprefix $(PATH_OBJ_UTILS), $(addsuffix .o, $(FILES_UTILS)))
SRC_UTILS = $(addprefix $(PATH_SRC_UTILS), $(addsuffix .c, $(FILES_UTILS)))

#******************************************************************************#
#                                    LIBFT                                     #
#******************************************************************************#

PATH_LIBFT = ./LIBFT/
NAME_LIBFT = ft
LIBFT = $(PATH_LIBFT)lib$(NAME_LIBFT).a
ILIBFT = -L $(PATH_LIBFT) -l $(NAME_LIBFT)

#******************************************************************************#
#                                    ALL                                       #
#******************************************************************************#

PATHS_OBJ = $(PATH_OBJ) $(PATH_OBJ_LEM-IN) $(PATH_OBJ_READ) $(PATH_OBJ_UTILS) $(PATH_OBJ_SOLVE) $(PATH_OBJ_OUTPUT)
OBJS = $(OBJ_LEM-IN) $(OBJ_READ) $(OBJ_UTILS) $(OBJ_SOLVE) $(OBJ_OUTPUT)
SRCS = $(SRC_LEM-IN) $(SRC_READ) $(SRC_UTILS) $(SRC_SOLVE) $(SRC_OUTPUT)
FILES = $(FILES_LEM-IN) $(FILES_READ) $(FILES_UTILS) $(FILES_SOLVE) $(FILES_OUTPUT)

#******************************************************************************#
#                                    RULES                                     #
#******************************************************************************#

all: $(NAME)

clean:
	@echo "\n\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DES OBJETS🦄 🦄 🦄 🦄\033[0m\n"
	@rm -rf $(PATH_OBJ)
	@make clean -C $(PATH_LIBFT)

fclean:
	@echo "\n\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DE $(NAME)🦄 🦄 🦄 🦄\033[0m\n"
	@rm -f $(NAME)
	@rm -rf $(PATH_OBJ)
	@make fclean -C $(PATH_LIBFT)

re: fclean all

test: all
	@cat $(MAP)
	@echo "-----------------------------------"
	@./$(NAME) < $(MAP)

valgrind: all
	@valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -v ./$(NAME) < $(MAP)

debug: $(LIBFT)
	$(CC) -ggdb $(ILIBFT) $(SRCS) -I ./include -o debug-lem-in
	lldb ./debug-lem-in
	rm debug-lem-in
	rm -rf debug-lem-in.dSYM

#******************************************************************************#
#                             Compilation LIBFT                                #
#******************************************************************************#

$(NAME): $(LIBFT) $(PATHS_OBJ) $(OBJS)
	@echo "\n\033[1m🦄 🦄 🦄 🦄 CREATION DE LEM-IN🦄 🦄 🦄 🦄\033[0m\n"
	@$(CC) $(CC_FLAGS) $(ILIBFT) $(OBJS) -o $@
	@echo "  👍  👍  👍 \033[1mLEM-IN CREE\033[0m👍  👍  👍\n"

$(LIBFT):
	@make -C $(PATH_LIBFT)

$(PATHS_OBJ):
	@mkdir $@

$(PATH_OBJ)%.o: $(PATH_SRC)%.c $(INCS)
	@printf %b "0️⃣  Compilation de \033[1m$<\033[0m en \033[1m$@\033[0m..."
	@$(CC) $(CC_FLAGS) -o $@ -c $< -I $(PATH_INC)
	@printf "\r" && printf "                                                                                     \r"
