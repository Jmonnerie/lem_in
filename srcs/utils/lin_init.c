/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_init.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/05/02 20:14:47 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/07/25 21:31:27 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

t_solve			*lin_init_solve(int bottleneck)
{
	t_solve		*sol;

	if (!(sol = (t_solve *)ft_memalloc(sizeof(t_solve))) &&
		ft_printf("{red}ERROR: Malloc failed{eoc}\n"))
		return (FAIL);
	if (!(sol->tab = (t_room ***)ft_memalloc(sizeof(t_room **) * bottleneck)) &&
		ft_printf("{red}ERROR: Malloc failed{eoc}\n"))
		return (FAIL);
	if (!(sol->len = (int *)ft_memalloc(sizeof(int) * bottleneck)) &&
		ft_printf("{red}ERROR: Malloc failed{eoc}\n"))
		return (FAIL);
	return (sol);
}

int				lin_init_pipes(t_lemin *lin)
{
	t_room		*tmp;

	tmp = lin->rooms;
	while (tmp)
	{
		lin->nbroom += 1;
		tmp = tmp->next;
	}
	tmp = lin->rooms;
	while (tmp)
	{
		if (!(tmp->pipes = (t_room **)malloc(sizeof(t_room *) * REALLOC_SIZE)))
			return (ft_printf("{red}ERROR: Malloc failed{eoc}\n"));
		ft_bzero(tmp->pipes, sizeof(t_room *) * REALLOC_SIZE);
		tmp = tmp->next;
	}
	return (SUCCESS);
}

t_room			*lin_init_room(char *name, int x, int y)
{
	t_room		*nroom;

	if (!(nroom = (t_room *)malloc(sizeof(t_room))) &&
		ft_printf("{red}ERROR: Malloc failed{eoc}\n"))
		return (FAIL);
	ft_bzero(nroom, sizeof(t_room));
	if (name)
	{
		if (!(nroom->name = ft_strdup(name)) &&
			ft_printf("{red}ERROR: Malloc failed{eoc}\n"))
			return (FAIL);
		nroom->x = x;
		nroom->y = y;
	}
	return (nroom);
}

int				lin_init(t_lemin **lin)
{
	t_lemin		*new;

	if (!(new = (t_lemin *)ft_memalloc(sizeof(t_lemin))) &&
		ft_printf("{red}ERROR: Malloc failed{eoc}\n"))
		return (FAIL);
	*lin = new;
	return (SUCCESS);
}
