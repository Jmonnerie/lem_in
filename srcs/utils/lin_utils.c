/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_utils.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/25 16:44:21 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/09/01 15:49:18 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

t_room			*lin_get_room_in_lst(t_room *lst, char *name)
{
	while (lst)
	{
		if (!ft_strcmp(lst->name, name))
			return (lst);
		lst = lst->next;
	}
	return (NULL);
}

int				lin_calc_turns(int ants, t_solve *sol, int nbways)
{
	int			turns;
	int			i;

	i = -1;
	turns = ants;
	while (++i < nbways)
		turns += sol->len[i];
	turns /= nbways;
	return (turns + (turns % nbways ? 1 : 0));
}

t_solve			*lin_copy_solution(t_solve *sol)
{
	int			i;
	t_solve		*nsol;

	if (!(nsol = (t_solve *)ft_memalloc(sizeof(t_solve))))
		return (FAIL);
	ft_memcpy(nsol, sol, sizeof(t_solve));
	if (!(nsol->len = (int *)ft_memalloc(sizeof(int) * sol->nbways)))
		return (FAIL);
	ft_memcpy(nsol->len, sol->len, sizeof(int) * sol->nbways);
	if (!(nsol->tab = (t_room ***)ft_memalloc(sizeof(t_room **) * sol->nbways)))
		return (FAIL);
	i = -1;
	while (++i < sol->nbways)
	{
		if (!(nsol->tab[i] =
					(t_room **)ft_memalloc(sizeof(t_room *) * sol->len[i] + 1)))
			return (FAIL);
		ft_memcpy(nsol->tab[i], sol->tab[i],
											sizeof(t_room *) * sol->len[i] + 1);
	}
	return (nsol);
}

int				lin_resize_pipes(t_room *rooma, t_room *roomb)
{
	t_room		**tab;

	if (rooma->npipes % REALLOC_SIZE == 0)
	{
		if (!(tab = (t_room **)ft_memalloc(sizeof(t_room *) *
										(rooma->npipes + REALLOC_SIZE + 1))))
			return (ft_printf("{red}ERROR: Malloc error{eoc}\n"));
		ft_memcpy(tab, rooma->pipes, sizeof(t_room *) * rooma->npipes);
		free(rooma->pipes);
		rooma->pipes = tab;
	}
	if (roomb->npipes % REALLOC_SIZE == 0)
	{
		if (!(tab = (t_room **)ft_memalloc(sizeof(t_room *) *
										(roomb->npipes + REALLOC_SIZE + 1))))
			return (ft_printf("{red}ERROR: Malloc error{eoc}\n"));
		ft_memcpy(tab, roomb->pipes, sizeof(t_room *) * roomb->npipes);
		free(roomb->pipes);
		roomb->pipes = tab;
	}
	rooma->pipes[rooma->npipes] = roomb;
	roomb->pipes[roomb->npipes] = rooma;
	rooma->npipes += 1;
	roomb->npipes += 1;
	return (SUCCESS);
}
