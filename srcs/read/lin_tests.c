/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_tests.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/03 18:38:11 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/07/25 16:47:07 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

int			lin_enought_infos(t_lemin *lin)
{
	if (!lin->start || !lin->end)
		return (!ft_printf("{red}ERROR: Not enought informations{eoc}\n"));
	if (lin->start->npipes == 0 || lin->end->npipes == 0)
		return (!ft_printf("{red}ERROR: Not enought informations{eoc}\n"));
	return (SUCCESS);
}

int			lin_is_a_room(char *line)
{
	int		i;

	i = -1;
	if (*line == 'L' || *line == '#' || *line == ' ')
		return (FAIL);
	while (line[++i] && line[i] != ' ')
		if (line[i] == '-')
			return (FAIL);
	if (!line[i])
		return (FAIL);
	while (line[++i] && ft_isdigit(line[i]))
		;
	if (!line[i] || line[i] != ' ')
		return (FAIL);
	if (line[++i] && ft_isdigit(line[i]))
		return (SUCCESS);
	return (FAIL);
}

int			lin_is_a_pipe(char *line)
{
	int		i;

	i = -1;
	if (*line == 'L' || *line == '#')
		return (FAIL);
	while (line[++i] != '-')
		if (line[i] == '\0' || line[i] == ' ')
			return (FAIL);
	while (line[++i] != '\0')
		if (line[i] == ' ' || line[i] == '-')
			return (FAIL);
	return (SUCCESS);
}
