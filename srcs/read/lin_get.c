/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_get.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/03 18:39:36 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/07/25 16:47:01 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

t_room			*lin_get_room(t_lemin *lin, char **line)
{
	char		**tab;
	t_room		*r;

	tab = ft_strsplit(*line, ' ');
	ft_printf("%s\n", *line);
	ft_strdel(line);
	if (lin_get_room_in_lst(lin->rooms, tab[0]) || **tab == 'L')
	{
		ft_printf("{red}ERROR: '%s' already exists or is incorrect{eoc}\n",
			tab[0]);
		ft_freestr2d(tab);
		return (NULL);
	}
	if (!(r = lin_init_room(tab[0], ft_atoi(tab[1]), ft_atoi(tab[2]))))
	{
		ft_freestr2d(tab);
		return (NULL);
	}
	ft_freestr2d(tab);
	return (r);
}

int				lin_get_pipe(t_lemin *lin, char **line)
{
	char		**tab;
	t_room		*rooma;
	t_room		*roomb;
	int			i;

	if (!(tab = ft_strsplit(*line, '-')) && ft_printf("%s\n", *line))
		return (!ft_printf("{red}ERROR: Malloc error{eoc}\n"));
	if (ft_strcmp(tab[0], tab[1]) == 0 && ft_printf("%s\n", *line))
		return (2);
	if (!(rooma = lin_get_room_in_lst(lin->rooms, tab[0])))
		return (1);
	if (!(roomb = lin_get_room_in_lst(lin->rooms, tab[1])))
		return (1);
	ft_printf("%s\n", *line);
	ft_freestr2d(tab);
	i = -1;
	while (rooma->pipes[++i])
		if (rooma->pipes[i] == roomb)
			return (2);
	if ((rooma == lin->start || rooma == lin->end) &&
								(roomb == lin->start || roomb == lin->end))
		lin->direct++;
	if (lin_resize_pipes(rooma, roomb) != SUCCESS)
		return (0);
	return (2);
}
