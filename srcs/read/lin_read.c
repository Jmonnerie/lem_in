/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_read.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/05/02 20:21:49 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/09/01 15:48:59 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

int				lin_comment(t_lemin *lin, char **line)
{
	ft_printf("%s\n", *line);
	if (ft_strcmp(*line, "##start") == 0 && ft_strdel(line))
	{
		if (!get_next_line(0, line) || !lin_is_a_room(*line))
			return (ft_printf("{red}ERROR: Incorrect room{eoc}\n"));
		if (!(lin->start = lin_get_room(lin, line)))
			return (FAIL);
		lin->start->next = lin->rooms;
		lin->rooms = lin->start;
	}
	else if (ft_strcmp(*line, "##end") == 0 && ft_strdel(line))
	{
		if (!get_next_line(0, line) || !lin_is_a_room(*line))
			return (ft_printf("{red}ERROR: Incorrect room{eoc}\n"));
		if (!(lin->end = lin_get_room(lin, line)))
			return (FAIL);
		lin->end->next = lin->rooms;
		lin->rooms = lin->end;
	}
	ft_strdel(line);
	return (SUCCESS);
}

int				lin_read_pipes(t_lemin *lin, char **line)
{
	int			ret;

	if (lin_init_pipes(lin) != SUCCESS)
		return (FAIL);
	if ((ret = lin_get_pipe(lin, line)) <= 1 && ft_strdel(line))
		return (ret);
	ft_strdel(line);
	while (get_next_line(0, line) > 0)
	{
		if (**line == '#' && ft_printf("%s\n", *line))
			ft_strdel(line);
		else if (lin_is_a_pipe(*line))
		{
			if ((ret = lin_get_pipe(lin, line)) <= 1 && ft_strdel(line))
				return (ret);
			ft_strdel(line);
		}
		else if (ft_strdel(line))
			return (SUCCESS);
	}
	return (SUCCESS);
}

int				lin_read_rooms(t_lemin *lin, char *line)
{
	t_room		*room;
	int			gnl;

	while ((gnl = get_next_line(0, &line)) > 0)
	{
		if (line[0] == '#')
		{
			if (lin_comment(lin, &line) != SUCCESS)
				return (FAIL);
		}
		else if (lin_is_a_room(line))
		{
			if (!(room = lin_get_room(lin, &line)))
				return (FAIL);
			room->next = lin->rooms;
			lin->rooms = room;
		}
		else if (lin_is_a_pipe(line))
			return (lin_read_pipes(lin, &line));
		else
			return (ft_printf("{red}ERROR: Not enought informations{eoc}\n"));
	}
	return (gnl == 0 ? ft_printf("{red}ERROR: Not enought informations{eoc}\n")
					: ft_printf("{red}ERROR: Read error{eoc}\n"));
}

int				lin_read_ants(t_lemin *lin)
{
	char		*line;

	line = NULL;
	if (!(get_next_line(0, &line)))
		return (ft_printf("{red}ERROR: Read error{eoc}\n"));
	lin->ant = ft_atoi(line);
	ft_printf("%s\n", line);
	ft_strdel(&line);
	return (lin->ant <= 0 ? ft_printf("{red}ERROR: Wrong ants number{eoc}\n")
		: SUCCESS);
}

int				lin_read(t_lemin *lin)
{
	if (lin_read_ants(lin) != SUCCESS)
		return (FAIL);
	if (lin_read_rooms(lin, NULL) != SUCCESS)
		return (FAIL);
	if (lin_enought_infos(lin) != SUCCESS)
		return (FAIL);
	return (SUCCESS);
}
