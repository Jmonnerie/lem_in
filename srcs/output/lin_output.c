/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_output.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/03 18:39:36 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/09/01 15:43:39 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

static int		lin_generate_ants_id(t_room ***tab, t_solve *sol,
				int *wrote, int ants)
{
	static int	i = 0;
	int			way;

	way = -1;
	while (++way < sol->nbways)
	{
		if (sol->nbturn - sol->len[way] > 0 && i < ants)
		{
			tab[way][0]->ant = ++i;
			if (*wrote > 0)
				ft_putchar(' ');
			(*wrote)++;
			ft_printf("L%d-%s", i, tab[way][0]->name);
		}
	}
	sol->nbturn--;
	return (SUCCESS);
}

int				lin_fuck_you_norm(t_room ***sol, int way, int *i, int *wrote)
{
	while (--(*i) >= 0)
	{
		if (sol[way][*i]->ant)
		{
			if (*wrote > 0)
				ft_putchar(' ');
			ft_printf("L%d-%s",
							sol[way][*i]->ant, sol[way][*i + 1]->name);
			(*wrote)++;
			sol[way][*i + 1]->ant = sol[way][*i]->ant;
			sol[way][*i]->ant = 0;
		}
	}
	return (SUCCESS);
}

int				lin_output(t_lemin *lin, t_room ***sol)
{
	int			way;
	int			i;
	int			wrote;

	while (lin->solutions->nbturn > 0)
	{
		ft_putchar('\n');
		way = -1;
		wrote = 0;
		while (++way < lin->solutions->nbways)
		{
			i = lin->solutions->len[way];
			lin->end->ant = 0;
			lin_fuck_you_norm(sol, way, &i, &wrote);
		}
		lin_generate_ants_id(sol, lin->solutions, &wrote, lin->ant);
	}
	return (SUCCESS);
}
