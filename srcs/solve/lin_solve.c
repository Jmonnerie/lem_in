/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_solve.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/25 16:46:46 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/09/01 15:49:31 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

int			lin_save_and_relaunch(t_lemin *lin, t_solve *sol)
{
	sol->nbturn = lin_calc_turns(lin->ant, sol, sol->nbways);
	if (sol->next && sol->nbturn >= sol->next->nbturn)
		return (SUCCESS);
	sol->copied++;
	if (sol->next)
		lin_free_solutions(sol->next);
	if (!(sol->next = lin_copy_solution(sol)))
		return (ft_printf("{red}ERROR: Malloc failed{eoc}\n"));
	return (SUCCESS);
}

int			lin_condition(t_lemin *lin)
{
	if (!lin->solutions->next)
	{
		if (lin->max >= (lin_calc_turns(lin->ant, lin->solutions,
			lin->solutions->nbways + 1) + lin->end->floor) / 2)
			return (SUCCESS);
		return (FAIL);
	}
	if (lin->solutions->next->nbturn >= (lin->solutions->nbturn =
		lin_calc_turns(lin->ant, lin->solutions, lin->solutions->nbways + 1)))
		return (SUCCESS);
	return (FAIL);
}

int			lin_dijkstra(t_lemin *lin, t_room *current, t_room **tab, int *len)
{
	int			i;

	*tab = current;
	(*len)++;
	if (lin_condition(lin))
	{
		if (current == lin->end && ++(lin->solutions->nbways))
			return (lin_recurse(lin, -1));
		current->visited++;
		i = -1;
		while (++i < current->npipes)
		{
			if (!current->pipes[i]->visited)
				if (lin_dijkstra(lin, current->pipes[i], tab + 1, len)
					!= SUCCESS)
					return (FAIL);
		}
		(current->visited)--;
	}
	(*len)--;
	*tab = NULL;
	return (SUCCESS);
}

int			lin_recurse(t_lemin *lin, int i)
{
	t_room		**tab;

	if (lin->solutions->nbways &&
		lin_save_and_relaunch(lin, lin->solutions) != SUCCESS)
		return (FAIL);
	if (!lin->solutions->nbways || (lin->solutions->copied &&
		lin->solutions->copied-- && lin->solutions->nbways < lin->bottleneck))
	{
		if (!(tab = (t_room **)ft_memalloc(sizeof(t_room *) * lin->nbroom)))
			return (ft_printf("{red}ERROR: Malloc failed{eoc}\n"));
		lin->solutions->tab[lin->solutions->nbways] = tab;
		while (lin->start->pipes[++i])
			if (!lin->start->pipes[i]->visited)
				lin_dijkstra(lin, lin->start->pipes[i], tab,
				lin->solutions->len + lin->solutions->nbways);
		free(tab);
	}
	if (lin->solutions->nbways)
		lin->solutions->nbways--;
	(lin->solutions->len[lin->solutions->nbways])--;
	return (SUCCESS);
}

int			lin_launcher_recurse(t_lemin *lin)
{
	t_solve		*tmp;

	lin->start->visited++;
	lin->bottleneck = lin->start->npipes < lin->end->npipes ?
		lin->start->npipes : lin->end->npipes;
	if (!(lin->solutions = lin_init_solve(lin->bottleneck)))
		return (FAIL);
	if (lin_recurse(lin, -1) != SUCCESS)
		return (FAIL);
	if (!lin->solutions->next)
		return (!ft_printf("{red}ERROR: Not enought informations{eoc}\n"));
	tmp = lin->solutions->next;
	lin_free_solutions(lin->solutions);
	lin->solutions = tmp;
	lin->solutions->next = NULL;
	return (lin->solutions ? SUCCESS : FAIL);
}
