/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lin_engine.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <jmonneri@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/05/02 20:17:46 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/09/01 15:42:31 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

void			lin_switch_tabs(int nbroom, t_room ***tab1, t_room ***tab2)
{
	t_room		**tmp;

	ft_bzero(*tab1, sizeof(t_room *) * (nbroom + 1));
	tmp = *tab1;
	*tab1 = *tab2;
	*tab2 = tmp;
}

int				lin_get_shortest_solution(t_lemin *lin, t_room **tab1,
				t_room **tab2, int floor)
{
	int			i;
	int			j;
	int			k;

	tab1[0] = lin->start;
	while (lin->end->floor == 0)
	{
		i = -1;
		k = 0;
		while (tab1[++i])
		{
			j = -1;
			while (++j < tab1[i]->npipes)
				if (tab1[i]->pipes[j]->floor == 0 &&
											(tab1[i]->pipes[j]->floor = floor))
					tab2[k++] = tab1[i]->pipes[j];
		}
		if (k == 0)
			return (!ft_printf("{red}ERROR: Not enought informations{eoc}\n"));
		lin_switch_tabs(lin->nbroom, &tab1, &tab2);
		floor++;
	}
	return (SUCCESS);
}

int				lin_larger_launcher(t_lemin *lin)
{
	t_room		**tab1;
	t_room		**tab2;

	if (!(tab1 = (t_room **)ft_memalloc(sizeof(t_room *) * lin->nbroom + 1)) ||
		!(tab2 = (t_room **)ft_memalloc(sizeof(t_room *) * lin->nbroom + 1)))
		return (!ft_printf("{red}ERROR: Malloc failed{eoc}\n"));
	lin_get_shortest_solution(lin, tab1, tab2, 1);
	lin->max = lin->ant + lin->end->floor + 1;
	free(tab1);
	free(tab2);
	return (SUCCESS);
}

int				lin_engine(t_lemin *lin)
{
	int			i;

	i = 0;
	if (lin_read(lin) != SUCCESS)
		return (FAIL);
	if (lin->direct)
	{
		ft_putchar('\n');
		while (++i < lin->ant)
			ft_printf("L%d-%s ", i, lin->end->name);
		return (!!ft_printf("L%d-%s\n", i, lin->end->name));
	}
	if (lin_larger_launcher(lin) != SUCCESS)
		return (FAIL);
	if (lin_launcher_recurse(lin) != SUCCESS)
		return (FAIL);
	if (lin_output(lin, lin->solutions->tab) != SUCCESS)
		return (FAIL);
	return (SUCCESS);
}
