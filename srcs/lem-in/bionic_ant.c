/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bionic_ant.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/07/25 16:44:21 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/07/25 21:46:11 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lemin.h"

int					lin_free_rooms(t_room *rooms)
{
	t_room			*tmp;

	while (rooms)
	{
		tmp = rooms->next;
		free(rooms->pipes);
		free(rooms->name);
		free(rooms);
		rooms = tmp;
	}
	return (SUCCESS);
}

int					lin_free_solutions(t_solve *sol)
{
	int				i;

	i = -1;
	if (!sol)
		return (SUCCESS);
	while (++i < sol->nbways)
		free(sol->tab[i]);
	if (sol->tab)
		free(sol->tab);
	if (sol->len)
		free(sol->len);
	free(sol);
	return (SUCCESS);
}

int					lin_free(t_lemin *lin, int ret)
{
	if (!lin)
		return (ret);
	if (lin->solutions)
		lin_free_solutions(lin->solutions->next);
	lin_free_solutions(lin->solutions);
	lin_free_rooms(lin->rooms);
	free(lin);
	return (ret);
}

int					main(void)
{
	t_lemin			*lin;

	lin = NULL;
	if (!(lin_init(&lin)))
		return (-1);
	if (lin_engine(lin) != SUCCESS)
		return (lin_free(lin, -1));
	return (lin_free(lin, 0));
}
